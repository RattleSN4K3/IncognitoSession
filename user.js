// ==UserScript==
// @name            IncognitoSession
// @description     Stores the browsing history
// @version         0.0.2
//
// @author          RattleSN4K3
// @namespace       https://gitlab.com/RattleSN4K3
//
// @downloadURL     https://gitlab.com/RattleSN4K3/IncognitoSession/raw/master/user.js
// @updateURL       https://gitlab.com/RattleSN4K3/IncognitoSession/raw/master/user.js
//
// ==/UserScript==